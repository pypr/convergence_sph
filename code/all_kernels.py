from pysph.base.kernels import CubicSpline 
from pysph.solver.solver import Solver
from pysph.solver.application import Application
from pysph.base import kernels
import numpy as np
import os


class KernelAnalysis():
    def __init__(self, kernel, dx, h, dir):
        self.dx = dx
        self.dim = dim
        self.kernel = kernel
        self.h = h
        self.dir = dir

    def evaluate_value(self, dx):
        x = np.arange(0, 1, dx)
        j = np.where(abs(x - 0.5) == min(abs(x-0.5)))[0][-1]
        wij = []
        for xi in x:
            r = abs(x[j] - xi)
            h = self.h
            wij.append(self.kernel.kernel(xij=[0, 0, 0], rij=r, h=h))
        return x, wij

    def fourier_transform(self):
        x, wij = self.evaluate_value(self.dx)
        xplot, wijplot = self.evaluate_value(self.dx/20)

        wij_k = np.fft.fft(wij)
        k = np.fft.fftfreq(x.shape[-1], self.dx)

        return xplot, wijplot, k, wij_k

    def run(self):
        name = os.path.join(self.dir, 'results.npz')

        x, wij, k, wij_k = self.fourier_transform() 
        print(wij)
        np.savez(name, x=x, w=list(np.asarray(wij)), k=k, wk=wij_k)


if __name__ == '__main__':
    import argparse
    from pysph.solver.application import list_all_kernels
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "-d", "--directory", action="store", dest="output_dir",
        default=None, help="Dump output in the specified"
        "directory.")

    all_kernels = list_all_kernels()
    parser.add_argument(
        "--kernel", action="store", dest="kernel", default=None,
        choices=all_kernels, help="Use specified kernel from %s" %
        all_kernels)

    parser.add_argument(
        "--dx", action="store", dest="dx", default=0.01, type=float, help="spacing")

    parser.add_argument(
        "--h", action="store", dest="h", default=0.02, type=float,
        help="support of kernel")

    args = parser.parse_args()
    dim = 1
    kernel = getattr(kernels, args.kernel)(dim=dim)

    h = args.h
    dx = args.dx
    dir = args.output_dir

    complete = os.path.join(dir, 'complete')
    if os.path.exists(complete):
        import sys
        sys.exit()

    obj = KernelAnalysis(kernel, dx, h, dir)
    obj.run()

    fp = open(complete, 'w')
    fp.close()
