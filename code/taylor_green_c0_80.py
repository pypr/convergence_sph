
"""Taylor Green vortex flow (5 minutes).
"""

import os
import numpy as np
from numpy import pi, cos

from pysph.base.nnps import DomainManager
from pysph.base.utils import get_particle_array
from pysph.base.kernels import QuinticSpline
from pysph.solver.application import Application

from taylor_green import TaylorGreen
from tg_config import exact_solution

# domain and constants
L = 1.0
U = 1.0
rho0 = 1.0
c0 = 80 * U
p0 = c0**2 * rho0


class TaylorGreenC080(TaylorGreen):
    def consume_user_options(self):
        nx = self.options.nx
        re = self.options.re

        self.c0 = self.options.c0_fac * U
        self.nu = nu = U * L / re

        self.dx = dx = L / nx
        self.volume = dx * dx
        self.hdx = self.options.hdx

        h0 = self.hdx * self.dx
        if self.options.scheme.endswith('isph'):
            dt_cfl = 0.25 * dx / U
        else:
            dt_cfl = 0.25 * dx / (self.c0 + U)
        dt_viscous = 0.125 * dx**2 / nu
        dt_force = 0.25 * 1.0

        self.dt = min(dt_cfl, dt_viscous, dt_force)
        self.tf = 2.0
        self.kernel_correction = self.options.kernel_correction

    def configure_scheme(self):
        from tg_config import configure_scheme
        configure_scheme(self, p0)

    def create_scheme(self):
        from tg_config import create_scheme
        return create_scheme(rho0, c0, p0)


if __name__ == '__main__':
    app = TaylorGreenC080()
    app.run()
    app.post_process(app.info_filename)
