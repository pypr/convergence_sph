from compyle.api import declare
from pysph.sph.equation import Equation
from pysph.sph.wc.linalg import (
    gj_solve
)


class KGFPreStep(Equation):
    def __init__(self, dest, sources, dim=2):
        self.dim = dim
        super(KGFPreStep, self).__init__(dest, sources)

    def initialize(self, d_idx, d_L):
        i, idx = declare('int', 2)

        idx = 16 * d_idx
        for i in range(16):
            d_L[idx + i] = 0.0

    def loop(self, d_L, d_idx, s_m, s_rho, s_idx, RIJ, XIJ, HIJ,
             WIJ):
        V_j = s_m[s_idx] / s_rho[s_idx]

        idx, i, j, n = declare('int', 4)
        idx = 16*d_idx
        n = self.dim + 1
        # Append `1` to XIJ to compute zeroth moment.
        YJI = declare('matrix(4)')
        YJI[0] = 1.0
        YJI[1] = -XIJ[0]
        YJI[2] = -XIJ[1]
        YJI[3] = -XIJ[2]

        for i in range(n):
            for j in range(n):
                d_L[idx + 4*i + j] += V_j*WIJ*YJI[i]*YJI[j]


class KGFCorrection(Equation):
    def _get_helpers_(self):
        return [gj_solve]

    def __init__(self, dest, sources, dim=2, tol=0.1):
        self.dim = dim
        self.tol = tol
        super(KGFCorrection, self).__init__(dest, sources)

    def loop(self, d_idx, d_L, WIJ, XIJ, DWIJ, HIJ):
        i, j, n, nt = declare('int', 4)
        n = self.dim + 1
        nt = n + 1
        # Note that we allocate enough for a 3D case but may only use a
        # part of the matrix.
        temp = declare('matrix(20)')
        res = declare('matrix(4)')
        YJI = declare('matrix(4)')
        YJI[0] = 1.0
        YJI[1] = -XIJ[0]
        YJI[2] = -XIJ[1]
        YJI[3] = -XIJ[2]
        for i in range(n):
            for j in range(n):
                temp[nt * i + j] = d_L[16 * d_idx + 4 * i + j]
            # Augmented part of matrix
            temp[nt*i + n] = YJI[i] * WIJ

        error_code = gj_solve(temp, n, 1, res)

        eps = 1.0e-04 * HIJ
        res_mag = 0.0
        dwij_mag = 0.0
        for i in range(n):
            res_mag += abs(res[i])
            dwij_mag += abs(DWIJ[i])
        change = abs(res_mag - dwij_mag)/(dwij_mag + eps)
        if change < self.tol:
            for i in range(self.dim):
                DWIJ[i] = res[i+1]

    
class FirstOrderPreStep(Equation):
    # Liu et al 2005
    def __init__(self, dest, sources, dim=2):
        self.dim = dim

        super(FirstOrderPreStep, self).__init__(dest, sources)

    def initialize(self, d_idx, d_L):
        i, j = declare('int', 2)

        for i in range(4):
            for j in range(4):
                d_L[16*d_idx + j+4*i] = 0.0

    def loop(self, d_idx, s_idx, d_h, s_h, s_x, s_y, s_z, d_x, d_y, d_z, s_rho,
             s_m, WIJ, XIJ, DWIJ, d_L):
        Vj = s_m[s_idx] / s_rho[s_idx]
        i16 = declare('int')
        i16 = 16*d_idx

        d_L[i16+0] += WIJ * Vj

        d_L[i16+1] += -XIJ[0] * WIJ * Vj
        d_L[i16+2] += -XIJ[1] * WIJ * Vj
        d_L[i16+3] += -XIJ[2] * WIJ * Vj

        d_L[i16+4] += DWIJ[0] * Vj
        d_L[i16+8] += DWIJ[1] * Vj
        d_L[i16+12] += DWIJ[2] * Vj

        d_L[i16+5] += -XIJ[0] * DWIJ[0] * Vj
        d_L[i16+6] += -XIJ[1] * DWIJ[0] * Vj
        d_L[i16+7] += -XIJ[2] * DWIJ[0] * Vj

        d_L[i16+9] += - XIJ[0] * DWIJ[1] * Vj
        d_L[i16+10] += -XIJ[1] * DWIJ[1] * Vj
        d_L[i16+11] += -XIJ[2] * DWIJ[1] * Vj

        d_L[i16+13] += -XIJ[0] * DWIJ[2] * Vj
        d_L[i16+14] += -XIJ[1] * DWIJ[2] * Vj
        d_L[i16+15] += -XIJ[2] * DWIJ[2] * Vj


class FirstOrderCorrection(Equation):
    def _get_helpers_(self):
        return [gj_solve]

    def __init__(self, dest, sources, dim=2, tol=0.1):
        self.dim = dim
        self.tol = tol
        super(FirstOrderCorrection, self).__init__(dest, sources)

    def loop(self, d_idx, d_L, WIJ, XIJ, DWIJ, HIJ):
        i, j, n, nt = declare('int', 4)
        n = self.dim + 1
        nt = n + 1
        # Note that we allocate enough for a 3D case but may only use a
        # part of the matrix.
        temp = declare('matrix(20)')
        res = declare('matrix(4)')
        YJI = declare('matrix(4)')
        for i in range(n):
            for j in range(n):
                temp[nt * i + j] = d_L[16 * d_idx + 4 * i + j]
            # Augmented part of matrix
            if i == 0:
                temp[nt*i + n] = WIJ
            else:
                temp[nt*i + n] = DWIJ[i-1]

        error_code = gj_solve(temp, n, 1, res)

        for i in range(self.dim):
            DWIJ[i] = res[i+1]

