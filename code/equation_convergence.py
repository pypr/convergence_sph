import numpy as np
import os
from pysph.base.utils import get_particle_array
from pysph.solver.application import Application
from pysph.solver.solver import Solver
from pysph.sph.integrator_step import EulerStep
from pysph.sph.integrator import EulerIntegrator
from pysph.base.nnps import DomainManager
import matplotlib
from sph_approximations import get_equations
# matplotlib.use('tkagg')
import matplotlib.pyplot as plt

pi = np.pi
c0 = 10


class MyStep(EulerStep):
    # dummy stepper
    def stage1(self):
        pass

class PressureGradApprox(Application):
    def initialize(self):
        self.dim = 2
        self.dx = 0.01
        self.use_sph = None
        self.rand = 0
        self.derv = 0

        self.L = 1.0

    def add_user_options(self, group):
        group.add_argument(
            '--dim', action='store', dest='dim', type=int, default=2,
            help='dimesion of the problem')

        group.add_argument(
            '--hdx', action='store', dest='hdx', type=float, default=1.0,
            help='h/dx')

        group.add_argument(
            '--error', action='store', dest='error', type=float, default=1.0e-14, help='the required error')

        group.add_argument(
            '--N', action='store', dest='N', type=int, default=10,
            help='Number of particle in unit length')

        group.add_argument(
            "--use-sph", action="store", dest="use_sph",
            type=str, default='sph',
            help="sph, order1, monaghan1992, libersky"
            )

        group.add_argument(
            "--do-post", action="store", dest="do_post",
            type=int, default='0',
            help="if do_post=1 and perform post step"
            )

        group.add_argument(
            "--norm", action="store", dest="norm",
            type=str, default='l1',
            help="the used for hdx analysis"
            )

        group.add_argument(
            "--perturb", action="store", dest="perturb",
            type=int, default=0,
            help="if value greater the 0 the particles are perturbed"
            )

        group.add_argument(
            "--derv", action="store", dest="derv",
            type=int, default=0,
            help="if zero than find derivative"
            )

        group.add_argument(
            "--max-err", action="store", dest="max_err",
            type=float, default=1e-3,
            help="The maximum error in the SPH approximation"
            )

        group.add_argument(
            "--zero-div", action="store", dest="zero_div",
            type=str, default='n',
            help="Zero divergence velocity field is used"
            )
        
    def consume_user_options(self):
        self.hdx = self.options.hdx
        self.dim = self.options.dim
        self.rand = self.options.perturb
        self.derv = self.options.derv
        self.use_sph = self.options.use_sph
        self.error = self.options.error
        self.norm = self.options.norm
        self.N = self.options.N
        self.dx = self.L / self.N
        self.max_err = self.options.max_err
        self.do_post = self.options.do_post
        self.zero_div = self.options.zero_div

    def get_function(self, x, y):
        f = np.sin(2*pi*(x+y)/self.L) 
        return f

    def get_function_grad(self, x, y):
        fx = -2*pi/self.L * np.cos(2*pi*(x+y)/self.L) 
        fy = -2*pi/self.L * np.cos(2*pi*(x+y)/self.L) 
        return fx, fy

    def get_function_laplace(self, x, y):
        fx = -2* (2*pi/self.L)**2 * np.sin(2*pi*(x+y)/self.L)
        # fx = -2* (2*np.pi)**2 * np.sin(2*np.pi*(x+y))
        fy = -2*(2*pi/self.L)**2 * np.sin(2*pi*(x+y)/self.L)
        return fx, fy

    def get_function_div(self, x, y):
        f = -4*pi/self.L * np.cos(2*pi*(x+y)/self.L) 
        return f

    def create_particles(self):
        hdx = self.hdx
        print('create particle', hdx, self.dx)
        L = self.L
        dx = self.dx
        x, y = None, None
        if self.rand == 1:
            filename = '%d_tg.npz'%self.options.N
            dirname = os.path.dirname(os.path.abspath(__file__))
            print(dirname)
            datafile = os.path.join(os.path.dirname(dirname), 'data', filename)
            print(datafile)
            if os.path.exists(datafile):
                data = np.load(datafile)
                x = data['x']
                y = data['y']
                x += 0.5
                y += 0.5
                print(x, y)
        else:
            _x = np.arange(dx / 2, L, dx)
            x, y = np.meshgrid(_x, _x)
        x, y = [t.ravel() for t in (x, y)]

        if self.dim == 1:
            _x = np.arange(dx / 2, L, dx)
            x = _x
            y = np.zeros_like(_x)
            if self.rand == 1:
                x = x + (np.random.random(len(x)) + .5)/0.5 * self.dx/10


        # Source setup
        h = np.ones_like(x)*dx*hdx
        rho = np.ones_like(x)
        m = rho*dx**(self.dim)

        # src = get_particle_array(name="src", x=x, y=y, m=m, h=h, rho=rho)
        dest = get_particle_array(name="dest", x=x, y=y, h=h, m=m, rho=rho, V0=dx**self.dim, rhoc=rho)

        particles = [dest]

        name = self.use_sph.split('_')        
        props = ['V', 'u', 'v', 'w', 'p', 'cs', 'dt_cfl', 'dt_force', 'wij', 'pavg', 'rhoc', 'rhoc0', 'u0', 'v0', 'w0', 'x0', 'y0', 'z0', 'arho']
        stride_prop = {'dwij':3}
        if name[0] == 'ce':
            props += ['arho']
        elif (name[0] == 'me') or (name[0] == 'visc'):
            props += ['au', 'av', 'aw']
            props += ['auhat', 'avhat', 'awhat']

        if len(name) > 1:
            if name[1] == 'delta':
                stride_prop['gradrho'] = 3
            if name[1] == 'coupled' or name[1] == 'fatehi':
                stride_prop['gradv'] = 9
                stride_prop['m_mat'] = 9
            if name[1] == 'korzilius':
                props += ['gamma2']
                stride_prop['m_mat'] = 9
                stride_prop['bt'] = 9
                stride_prop['gradv'] = 9
                stride_prop['L'] = 9
                stride_prop['gamma'] = 9
                stride_prop['ddu'] = 3

        # props for kernel correction
        if len(name) > 1:
            if (name[-1] == 'bonet'):
                stride_prop['m_mat'] = 9
            elif (name[-1] == 'liu') or (name[-1] == 'mc'):
                stride_prop['L'] = 16
            elif (name[-1] == 'zo'):
                props += ['xijdwij']
            elif (name[-1] == 'fatehi'):
                stride_prop['L'] = 9
                stride_prop['L1'] = 3
                stride_prop['L2'] = 9
                stride_prop['L3'] = 27
                stride_prop['bt'] = 9


        for pa in particles:
            for prop in props:
                pa.add_property(prop)
            for prop in stride_prop.keys():
                pa.add_property(prop, stride=stride_prop[prop])

        dest.u[:] = self.get_function(x, y)
        dest.v[:] = self.get_function(x, y)
        if self.zero_div == 'y':
            dest.u[:] = -np.cos(2*np.pi*x) * np.sin(2*np.pi*y)
            dest.v[:] = np.sin(2*np.pi*x) * np.cos(2*np.pi*y)

        dest.p[:] = self.get_function(x, y)
        if self.use_sph == 'visc':
            dest.p[:] = 0.0
        # Dest setup

        nfp = dest.get_number_of_particles()
        dest.gid[:] = np.arange(nfp)

        props += ['x', 'y', 'z', 'm', 'h', 'rho']
        if len(name) > 1:
            if name[1] == 'korzilius':
                props += ['ddu']
        for pa_arr in particles:
            pa_arr.set_output_arrays(props)

        return particles 

    def create_domain(self):
        L = self.L
        if self.dim == 1:
            return DomainManager(
                xmin=0, xmax=L, periodic_in_x=True)
        elif self.dim == 2:
            return DomainManager(
                xmin=0, xmax=L, ymin=0, ymax=L, periodic_in_x=True,
                periodic_in_y=True)

    def create_equations(self):
        from scheme_equation import get_equation
        eqns = get_equation(self)
        print(eqns)
        return eqns

    def create_solver(self):
        integrator = EulerIntegrator(dest=MyStep())
        return Solver(dim=self.dim,
                      pfreq=1,
                      integrator=integrator,
                      tf=1.0,
                      dt=1.0)

    def post_process(self, info):
        from pysph.solver.utils import load
        self.read_info(info)
        if len(self.output_files) == 0:
            return

        data = load(self.output_files[-1])
        dest = data['arrays']['dest']
        x = dest.x
        y = dest.y
        fc = None
        fc1 = None
        fe = None

        method = self.use_sph
        name = self.use_sph.split('_')        
        print(name)

        if name[0] == 'ce': 
            fc = dest.arho
            fc1 = dest.arho
            fe = self.get_function_div(x, y)
            if self.zero_div == 'y':
                fe = np.zeros_like(x)
        elif name[0] == 'me': 
            fe, fe1 = self.get_function_grad(x, y)
            fc = dest.au 
            fc1 = dest.av 
            fe /= dest.rho
            fe1 /= dest.rho
        elif name[0] == 'visc': 
            fe, fe1 = self.get_function_laplace(x, y)
            if method == 'visc_korzilius':
                fc = dest.ddu[0::3]
                fc1 = dest.ddu[2::3]
                fc = fc + fc1
                print(fc, fc1)
            else:
                fc = dest.au 
                fc1 = dest.av
            # if (self.use_sph == 'visc') or (self.use_sph == 'visc_bonet') or (self.use_sph == 'visc_cleary') or (self.use_sph == 'visc_cleary_bonet'):
            #     fe *= dest.rho
            #     fe1 *= dest.rho

        V = dest.m/dest.rho 
        ang_mom = sum(fc * y - fc1 * x)
        cond = (x > 0.5) & (x < 0.75) & (y > 0.5) & ( y < 0.75)
        print(fe[cond], fc[cond], sum(fc), max(fc), sum(fc)/max(fc), ang_mom)
        print(sum(abs(fe[cond] - fc[cond])/len(fe[cond])))
        # plt.plot(fe[1000:1100], label='exact')
        # plt.plot(fc[1000:1100], label='comp')
        # plt.legend()
        # plt.show()
        filename = os.path.join(self.output_dir, 'results.npz')
        print(filename)
        np.savez(filename, x=x, y=y, fc=fc, fe=fe)



if __name__ == "__main__":
    app = PressureGradApprox()
    app.run()
    app.post_process(app.info_filename)