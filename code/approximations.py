import numpy as np
import os
from pysph.base.utils import get_particle_array
from pysph.solver.application import Application
from pysph.solver.solver import Solver
from pysph.sph.integrator_step import EulerStep
from pysph.sph.integrator import EulerIntegrator
from pysph.base.nnps import DomainManager
import matplotlib
from sph_approximations import get_equations
matplotlib.use('agg')
import matplotlib.pyplot as plt

pi = np.pi


class MyStep(EulerStep):
    # dummy stepper
    def stage1(self):
        pass


class SPHApprox(Application):
    def initialize(self):
        self.dim = 2
        self.dx = 0.01
        self.use_sph = None
        self.rand = 0
        self.derv = 0

        self.L = 1.0

    def add_user_options(self, group):
        group.add_argument(
            '--dim', action='store', dest='dim', type=int, default=2,
            help='dimesion of the problem')

        group.add_argument(
            '--hdx', action='store', dest='hdx', type=float, default=1.0,
            help='h/dx')

        group.add_argument(
            '--error', action='store', dest='error', type=float, default=1.0e-14, help='the required error')

        group.add_argument(
            '--N', action='store', dest='N', type=int, default=10,
            help='Number of particle in unit length')

        group.add_argument(
            "--use-sph", action="store", dest="use_sph",
            type=str, default='sph',
            help="sph, order1, monaghan1992, libersky"
            )

        group.add_argument(
            "--norm", action="store", dest="norm",
            type=str, default='l1',
            help="the used for hdx analysis"
            )

        group.add_argument(
            "--perturb", action="store", dest="perturb",
            type=int, default=0,
            help="if value greater the 0 the particles are perturbed"
            )

        group.add_argument(
            "--derv", action="store", dest="derv",
            type=int, default=0,
            help="if zero than find derivative"
            )

        group.add_argument(
            "--max-err", action="store", dest="max_err",
            type=float, default=1e-3,
            help="The maximum error in the SPH approximation"
            )

    def consume_user_options(self):
        self.hdx = self.options.hdx
        self.dim = self.options.dim
        self.rand = self.options.perturb
        self.derv = self.options.derv
        self.use_sph = self.options.use_sph
        self.error = self.options.error
        self.norm = self.options.norm
        self.N = self.options.N
        self.dx = self.L / self.N
        self.max_err = self.options.max_err

    def get_function(self, x, y):
        f = np.sin(2*pi*(x+y)/self.L) 
        return f

    def get_function_derv(self, x, y):
        f = 2*pi/self.L * np.cos(2*pi*(x+y)/self.L) 
        return f

    def create_particles(self):
        hdx = self.hdx
        print('create particle', hdx, self.dx)
        L = self.L
        dx = self.dx
        # x = np.arange(dx/2, L, dx)
        # y = 0
        x, y = None, None
        if self.rand == 1:
            filename = '%d_tg.npz'%self.options.N
            dirname = os.path.dirname(os.path.abspath(__file__))
            print(dirname)
            datafile = os.path.join(os.path.dirname(dirname), 'data', filename)
            print(datafile)
            if os.path.exists(datafile):
                data = np.load(datafile)
                x = data['x']
                y = data['y']
                x += 0.5
                y += 0.5
                print(x, y)
        else:
            _x = np.arange(dx / 2, L, dx)
            x, y = np.meshgrid(_x, _x)
        x, y = [t.ravel() for t in (x, y)]
        #     x, y = np.mgrid[-L+dx/2:L:dx, -L+dx/2:L:dx]
        #     x, y = [t.ravel() for t in (x, y)]

        # if self.rand == 1:
        #     np.random.seed(10)
        #     x = x + (np.random.random(len(x))-0.5)*dx/4
        #     if self.dim == 2:
        #         y = y + (np.random.random(len(y))-0.5)*dx/4

        # Source setup
        prop = self.get_function(x, y)
        h = np.ones_like(x)*dx*hdx
        rho = np.ones_like(x)
        m = rho*dx**(self.dim)
        src = get_particle_array(name="src", x=x, y=y, m=m, h=h, rho=rho)

        src.add_property(name="prop", type="double")
        src.add_property(name="V")
        src.add_property(name="nnbr")
        src.prop = prop

        # Dest setup
        # filter = int(len(x)/1000)
        # xnew = x[::filter]
        # ynew = y[::filter]
        # dest = get_particle_array(name="dest", x=xnew, y=ynew, h=h[0], m=m[0], rho=rho[0])
        # print(len(dest.x))
        dest = get_particle_array(name="dest", x=x, y=y, h=h, m=m, rho=rho)
        if (self.use_sph == 'order1') or (self.use_sph == 'kgf'):
            dest.add_property('moment', stride=16)
            dest.add_property('p_sph', stride=4)
            dest.add_property('prop', stride=4)
            src.add_property("temp_prop")
            src.temp_prop[:] = prop.copy()
        elif (self.use_sph == 'fia'):
            dest.add_property('moment', stride=9)
            dest.add_property('p_sph', stride=3)
            dest.add_property('prop', stride=3)
            src.add_property("temp_prop")
            dest.add_property("temp_prop")
            src.temp_prop[:] = prop.copy()
            dest.temp_prop[:] = prop.copy()
        elif self.use_sph == 'libersky':
            dest.add_property("m1")
            src.add_property("rho_n")
            src.add_property("wij")
        elif self.use_sph == 'csph':
            dest.add_property("wij")
            dest.add_property("m1")
        elif (self.use_sph == 'kc') or (self.use_sph == 'mkc'):
            dest.add_property("wij")
            dest.add_property("cwij")
            dest.add_property('m_mat', stride=16)
            dest.add_property('dw_gamma', stride=3)
        elif self.use_sph == 'sd' or self.use_sph=='hda':
            dest.add_property('nnbr')
            src.add_property('nnbr')

        dest.add_property("prop", type="double")
        dest.add_property("f")

        props = ['x', 'y', 'z', 'm', 'h', 'rho', 'prop']
        if self.use_sph == 'sd':
            props += ['nnbr']
        for pa_arr in (src, dest):
            pa_arr.set_output_arrays(props)

        return [src, dest]

    def create_domain(self):
        L = self.L
        if self.dim == 1:
            return DomainManager(
                xmin=0, xmax=L, periodic_in_x=True)
        elif self.dim == 2:
            return DomainManager(
                xmin=0, xmax=L, ymin=0, ymax=L, periodic_in_x=True,
                periodic_in_y=True)

    def create_equations(self):
        import json
        basefolder = os.path.dirname(os.path.dirname(os.path.dirname(self.output_dir)))
        filename = os.path.join(basefolder, 'data', 'vol.json')
        data = {}
        vref = 0.0
        if os.path.exists(filename):
            data = json.load(open(filename, 'r')) 
        key = '%s_%d_%.2f'%(self.options.kernel, self.N, self.hdx)
        if key in data.keys():
            vref = data[key]
        if self.use_sph == 'sph' and self.rand == 1:
            self.use_sph = 'monaghan1992'
        eqns = get_equations(
            method=self.use_sph, derv=self.derv, dim=self.dim,
            err=self.error, norm=self.norm, N=self.N, vref=vref, 
            max_err=self.max_err)
        print(eqns)
        return eqns

    def create_solver(self):
        integrator = EulerIntegrator(dest=MyStep())
        return Solver(dim=self.dim,
                      pfreq=1,
                      integrator=integrator,
                      tf=1.0,
                      dt=1.0)

    def post_process(self, info):
        from pysph.solver.utils import load
        self.read_info(info)
        if len(self.output_files) == 0:
            return

        if self.use_sph == 'sd':
            self.sd_pp()
            return 
        if self.use_sph == 'nd':
            self.nd_pp()
            return 
        elif self.use_sph == 'hda':
            self.hda_pp()
            return 
        elif self.use_sph == 'save_h':
            self.save_hdx()
            return 

        data = load(self.output_files[-1])
        dest = data['arrays']['dest']
        x = dest.x
        y = dest.y
        # dx = self.dx
        # fac = 3 * self.hdx
        # condx =  (x < max(x) - fac * dx) & ( x> min(x) + fac * dx)
        # condy =  (y < max(y) - fac * dx) & ( y> min(y) + fac * dx)
        # cond = condx & condy

        f = self.get_function(x, y)
        if self.derv:
            f = self.get_function_derv(x, y)
        f_comp = dest.prop
        if (self.use_sph == 'order1') or (self.use_sph == 'kgf'):
            f_comp = dest.prop[self.derv::4].copy()
        elif self.use_sph == 'fia':
            f_comp = dest.prop[self.derv::3].copy()

        filename = os.path.join(self.output_dir, 'results.npz')
        # np.savez(filename, fe=f[cond], fc=f_comp[cond])
        np.savez(filename, fe=f, fc=f_comp)
        print(f_comp, f)

        d = abs(np.sqrt(x**2 + y**2) - 0.5)
        id = np.where(d == min(d))[0][-1]

        print(id)
        npts = 45
        # plt.plot(f)
        # plt.plot(f_comp)
        plt.plot(f[id-npts:id+npts], label='exact')
        plt.plot(f_comp[id-npts:id+npts], label='computed')
        plt.legend()
        plt.grid()

        filename = os.path.join(self.output_dir, 'approx.png')
        plt.savefig(filename, dpi=300)
        plt.close()

    def sd_pp(self):
        from pysph.solver.utils import load
        data = load(self.output_files[-1])
        dest = data['arrays']['dest']

        rho = dest.prop
        nnbr = dest.nnbr
        print(nnbr, rho)

        filename = os.path.join(self.output_dir, 'results.npz')
        np.savez(filename, rho=rho, nnbr=nnbr)

    def nd_pp(self):
        from pysph.solver.utils import load
        import json
        data = load(self.output_files[-1])
        dest = data['arrays']['dest']

        nd = dest.prop[0]

        basefolder = os.path.dirname(os.path.dirname(os.path.dirname(self.output_dir)))
        filename = os.path.join(basefolder, 'data', 'vol.json')
        data = {}
        if os.path.exists(filename):
            data = json.load(open(filename, 'r')) 
        key = '%s_%d_%.2f'%(self.solver.kernel.__class__.__name__, self.N, self.hdx)
        if key not in data.keys():
            data[key] = nd

        json.dump(data, open(filename, 'w'))
        
    def save_hdx(self):
        from pysph.solver.utils import load
        import json
        data = load(self.output_files[-1])
        dest = data['arrays']['dest']

        x = dest.x
        y = dest.y
        h = dest.h

        basefolder = os.path.dirname(os.path.dirname(os.path.dirname(self.output_dir)))
        filename = os.path.join(basefolder, 'data', '%s_%d_%.2f.npz'%(self.options.kernel, self.N, self.hdx))
        np.savez(filename, x=x, y=y, h=h)

        
    def hda_pp(self):
        from pysph.solver.utils import load
        data = load(self.output_files[-1])
        dest = data['arrays']['dest']

        x = dest.x
        y = dest.y

        rho = dest.prop - self.get_function(x, y) 
        hdx = dest.h/self.dx
        print(hdx, rho)

        filename = os.path.join(self.output_dir, 'results.npz')
        np.savez(filename, rho=rho, hdx=hdx)


if __name__ == "__main__":
    app = SPHApprox()
    app.run()
    app.post_process(app.info_filename)