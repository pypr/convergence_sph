import os
from numpy import pi, cos

from pysph.base.nnps import DomainManager
from pysph.base.utils import get_particle_array
from pysph.solver.application import Application
from pysph.sph.equation import Equation
from taylor_green import TaylorGreen

import numpy as np

U = 1.0
L = 1.0
rho0 = 1.0
c0 = 10 * U
p0 = c0**2 * rho0


class GreshoChan(TaylorGreen):
    def consume_user_options(self):
        nx = self.options.nx
        re = self.options.re

        self.c0 = self.options.c0_fac * U
        self.nu = 0.0

        self.dx = dx = L / nx
        self.volume = dx * dx
        self.hdx = self.options.hdx

        h0 = self.hdx * self.dx
        if self.options.scheme.endswith('isph'):
            dt_cfl = 0.25 * dx / U
        else:
            dt_cfl = 0.25 * dx / (self.c0 + U)
        dt_force = 0.25 * 1.0

        self.dt = min(dt_cfl, dt_force)
        self.tf = 3.0
        self.kernel_correction = self.options.kernel_correction
        self.no_periodic = self.options.no_periodic

    def create_domain(self):
        if not self.options.no_periodic:
            return DomainManager(
                xmin=-L/2, xmax=L/2, ymin=-L/2, ymax=L/2, periodic_in_x=True,
                periodic_in_y=True
            )

    def exact_solution(self, U, x, y):
        from numpy import arctan2, cos, sin, log
        r = np.sqrt(x**2 + y**2)
        theta = np.arctan2(y, x)

        u = -5 * r * sin(theta)
        v = 5 * r * cos(theta)
        p = 12.5 * r**2 + 5

        cond = r > 0.2
        u[cond] = -(2 - 5*r[cond]) * sin(theta[cond])
        v[cond] = (2 - 5*r[cond]) * cos(theta[cond])
        p[cond] = 12.5*r[cond]**2 - 20*r[cond] + 4 * log(5*r[cond]) + 9

        cond = r>0.4
        u[cond] = 0.0
        v[cond] = 0.0
        p[cond] = 3 + 4 * np.log(2)

        return u, v, p

    def create_particles(self):
        # create the particles
        dx = self.dx
        x, y, h = None, None, None

        _x = np.arange(-L/2+dx / 2, L/2, dx)
        x, y = np.meshgrid(_x, _x)
        if self.options.init is not None:
            fname = self.options.init
            from pysph.solver.utils import load
            data = load(fname)
            _f = data['arrays']['fluid']
            x, y = _f.x.copy(), _f.y.copy()

        # Initialize
        m = self.volume * rho0
        V0 = self.volume
        if h is None:
            h = self.hdx * dx
        u0, v0, p0 = self.exact_solution(U, x, y)
        rhoc = None
        rho = rho0

        if self.options.eos == 'linear':
            print('linear')
            rho = (p0 / self.c0**2 + 1)
            rhoc = (p0 / self.c0**2 + 1)
        elif self.options.eos == 'tait':
            print('tait')
            rho = (p0 * 7.0 / self.c0**2 + 1)**(1./7.0)
            rhoc = (p0 * 7.0 / self.c0**2 + 1)**(1./7.0)

        # create the arrays
        fluid = get_particle_array(name='fluid', x=x, y=y, m=m, h=h, u=u0,
                                   v=v0, rho=rho, rhoc=rhoc, p=p0, V0=V0)

        self.scheme.setup_properties([fluid])

        print("Taylor green vortex problem :: nfluid = %d, dt = %g" % (
            fluid.get_number_of_particles(), self.dt))

        from tg_config import configure_particles
        configure_particles(self, fluid)

        nfp = fluid.get_number_of_particles()
        fluid.gid[:] = np.arange(nfp)
        fluid.add_constant('c0', self.c0)
        fluid.add_property('rhoc')
        fluid.add_property('V')

        return [fluid]

    def post_process(self, info_fname):
        if self.options.disbale_output:
            return
        self.read_info(info_fname)
        if len(self.output_files) == 0:
            return

        from pysph.solver.utils import iter_output
        from pysph.solver.utils import load
        from pysph.tools.interpolator import Interpolator
        decay_rate = -8.0 * np.pi**2 / self.options.re

        files = self.output_files

        data = load(files[-1])
        sd = data['solver_data']
        fluid = data['arrays']['fluid']

        time, acc, xe, ve, r, vmag, ve1, mom, a_mom  = [], [], [], [], [], [],[], [], []

        for sd, array in iter_output(files[0:], 'fluid'):
            _t = sd['t']
            x, y, m, u, v, p, au, av, T, rhoc, rho = self._get_post_process_props(array)
            time.append(_t)
            acc.append(abs(sum(au)))
            mom.append(sum(m*u))
            a_mom.append(sum(m*(v*x - u*y)))
        _t = sd['t']
        x, y, m, u, v, p, au, av, T, rhoc, rho = self._get_post_process_props(fluid)
        xe = np.linspace(0, 0.5, 100)
        ye = np.zeros_like(xe)
        u_e, v_e, p_e = self.exact_solution(U, xe, ye)
        u_e1, v_e1, p_e1 = self.exact_solution(U, x, y)
        r = np.sqrt(x**2 + y**2)
        vmag = np.sqrt(u**2 + v**2)
        ve = np.sqrt(u_e**2 + v_e**2)
        ve1 = np.sqrt(u_e1**2 + v_e1**2)

        time, acc, xe, ve, r, vmag, ve1, mom, a_mom = list(map(np.asarray, (time, acc, xe, ve, r, vmag, ve1, mom, a_mom)))

        fname = os.path.join(self.output_dir, 'results.npz')
        np.savez(
            fname, time=time, acc=acc, xe=xe, ve=ve, r=r, vmag=vmag, ve1=ve1, mom=mom, a_mom=a_mom
        )

        import matplotlib
        matplotlib.use('Agg')

        from matplotlib import pyplot as plt
        plt.clf()
        plt.grid()
        plt.plot(r, vmag, 'o', label="computed", mfc='none')
        plt.plot(xe, ve, '--', label="exact")
        plt.xlabel('r')
        plt.ylabel('vmag')
        plt.xlim([0, 0.5])
        plt.legend()
        plt.title('time = %.2f'%_t)
        fig = os.path.join(self.output_dir, "velocity.png")
        plt.savefig(fig, dpi=300)

        plt.clf()
        plt.grid()
        plt.semilogy(time, acc, label=r"$F_t$")
        plt.xlabel('time')
        plt.ylabel(r'$F_t$')
        plt.legend()
        plt.title('Momentum conservation')
        fig = os.path.join(self.output_dir, "me_conv.png")
        plt.savefig(fig, dpi=300)

    def customize_output(self):
        self._mayavi_config('''
        b = particle_arrays['fluid']
        b.scalar = 'vmag'
        ''')


if __name__ == '__main__':
    app = GreshoChan()
    app.run()
    app.post_process(app.info_filename)