import os
from numpy import pi, cos
from numpy.core.numeric import zeros_like
from pysph.base.kernels import QuinticSpline

from pysph.base.nnps import DomainManager
from pysph.base.utils import get_particle_array
from pysph.solver.application import Application
from pysph.sph.equation import Equation
from taylor_green import TaylorGreen

import numpy as np

U = 1.0
L = 1.0
rho0 = 1.0
c0 = 10 * U
p0 = c0**2 * rho0


class ShearLayer(TaylorGreen):
    def add_user_options(self, group):
        super(ShearLayer, self).add_user_options(group)

        group.add_argument(
            "--prob", action="store", type=str, default='tg',
            help="The test case name - 'tg', 'sl', 'gv' "
        )


    def consume_user_options(self):
        nx = self.options.nx
        re = self.options.re
        self.prob = self.options.prob

        self.c0 = self.options.c0_fac * U
        self.nu = nu = 1.0/10000 

        self.dx = dx = L / nx
        self.volume = dx * dx
        self.hdx = self.options.hdx

        h0 = self.hdx * self.dx
        if self.options.scheme.endswith('isph'):
            dt_cfl = 0.25 * dx / U
        else:
            dt_cfl = 0.25 * dx / (self.c0 + U)
        dt_force = 0.25 * 1.0

        self.dt = min(dt_cfl, dt_force)
        self.tf = 2.0
        self.kernel_correction = self.options.kernel_correction

    def exact_solution(self, U, x, y):
        '''
        [1]Y. Di, R. Li, T. Tang, and P. Zhang, “Moving Mesh Finite Element Methods for the Incompressible Navier--Stokes Equations,” SIAM J. Sci. Comput., vol. 26, no. 3, pp. 1036–1056, Jan. 2005, doi: 10.1137/030600643.
        '''
        from numpy import arctan2, cos, sin, log
        r = np.sqrt(x**2 + y**2)
        theta = np.arctan2(y, x)

        u0 = 1
        slope = 30
        pert = 0.05

        u = np.zeros_like(x)
        rho = np.zeros_like(x)
        rhoc0 = np.zeros_like(x)

        cond = (y >= 0) & (y < 0.5) 
        u[cond] = u0 * np.tanh(slope*(y[cond] - 1/4))

        cond = (y >= 0.5) 
        u[cond] = u0 * np.tanh(slope*(3/4 - y[cond])) 

        v = pert * u0 * np.sin(2 * np.pi * x)

        return u, v

    def create_particles(self):
        # create the particles
        dx = self.dx
        x, y, h = None, None, None

        _x = np.arange(dx / 2, L, dx)
        x, y = np.meshgrid(_x, _x)

        # Initialize
        m = self.volume * rho0
        V0 = self.volume
        if h is None:
            h = self.hdx * dx
        u0, v0 = self.exact_solution(U, x, y)
        # rhoc = rhoc0 
        rhoc = rho0 
        rho = rho0
        p0 =  2.5

        # create the arrays
        fluid = get_particle_array(name='fluid', x=x, y=y, m=m, h=h, u=u0,
                                   v=v0, rho=rho, rhoc=rhoc, p=p0, V0=V0)

        self.scheme.setup_properties([fluid])

        print("Taylor green vortex problem :: nfluid = %d, dt = %g" % (
            fluid.get_number_of_particles(), self.dt))

        from tg_config import configure_particles
        configure_particles(self, fluid)

        nfp = fluid.get_number_of_particles()
        fluid.gid[:] = np.arange(nfp)
        fluid.add_constant('c0', self.c0)
        fluid.add_property('rhoc')
        fluid.add_property('gradv', stride=9)
        fluid.add_property('V')

        fluid.add_output_arrays(['gradv'])

        return [fluid]

    def post_process(self, info_fname):
        self.read_info(info_fname)
        if len(self.output_files) == 0:
            return

        from pysph.solver.utils import iter_output
        from pysph.solver.utils import load
        from pysph.tools.interpolator import Interpolator
        decay_rate = -8.0 * np.pi**2 / self.options.re

        files = self.output_files

        data = load(files[-1])
        sd = data['solver_data']
        fluid = data['arrays']['fluid']

        time, acc, xe, ve, r, vmag, ve1, mom, a_mom  = [], [], [], [], [], [],[], [], []

        for sd, array in iter_output(files[0:], 'fluid'):
            _t = sd['t']
            x, y, m, u, v, p, au, av, T, rhoc, rho = self._get_post_process_props(array)
            time.append(_t)
            acc.append(abs(sum(au)))
            mom.append(sum(m*u))
            a_mom.append(sum(m*(v*x - u*y)))

        time, mom, a_mom = list(map(np.asarray, (time, mom, a_mom)))

        x, y = np.mgrid[0:L:100, 0:L:100]
        kernel = QuinticSpline(dim=2)
        interp = Interpolator(
            [fluid], kernel=kernel, x=x, y=y,
            domain_manager=self.create_domain(), method='order1')

        u = interp.interpolate('u')

        x, y, u = list(map(np.asarray, (x, y, u)))

        fname = os.path.join(self.output_dir, 'results.npz')
        np.savez(
            fname, x=x, y=y, u=u, time=time, mom=mom, a_mom=a_mom
        )

    def customize_output(self):
        self._mayavi_config('''
        b = particle_arrays['fluid']
        b.scalar = 'vmag'
        ''')


if __name__ == '__main__':
    app = ShearLayer()
    app.run()
    app.post_process(app.info_filename)