import numpy as np
import os
from pysph.base.utils import get_particle_array
from pysph.solver.application import Application
from pysph.solver.solver import Solver
from pysph.sph.integrator_step import IntegratorStep
from pysph.sph.integrator import EulerIntegrator
from pysph.base.nnps import DomainManager
import matplotlib
from sph_approximations import get_equations
# matplotlib.use('tkagg')
import matplotlib.pyplot as plt

from tsph_with_pst import RK2Integrator
from equation_convergence import PressureGradApprox

pi = np.pi
c0 = 10

class RK2StepperHD(IntegratorStep):
    def initialize(
        self, d_idx, d_x0, d_y0, d_z0, d_x, d_y, d_z,
        d_u0, d_v0, d_w0, d_u, d_v, d_w, d_rhoc, d_rhoc0
    ):
        d_u0[d_idx] = d_u[d_idx]
        d_v0[d_idx] = d_v[d_idx]
        d_w0[d_idx] = d_w[d_idx]

    def stage1(
        self, d_idx, d_x0, d_y0, d_z0, d_x, d_y, d_z,
        d_u0, d_v0, d_w0, d_u, d_v, d_w, d_au, d_av, d_aw,
        dt, d_rhoc, d_arho, d_rhoc0
    ):
        dtb2 = 0.5*dt
        d_u[d_idx] = d_u0[d_idx] + dtb2*d_au[d_idx]
        d_v[d_idx] = d_v0[d_idx] + dtb2*d_av[d_idx]
        d_w[d_idx] = d_w0[d_idx] + dtb2*d_aw[d_idx]

    def stage2(
        self, d_idx, d_x0, d_y0, d_z0, d_x, d_y, d_z,
        d_u0, d_v0, d_w0, d_u, d_v, d_w, d_au, d_av, d_aw,
        dt, d_rhoc, d_arho, d_rhoc0
    ):
        # Compute `U^{n+1}`
        d_u[d_idx] = d_u0[d_idx] + dt*d_au[d_idx]
        d_v[d_idx] = d_v0[d_idx] + dt*d_av[d_idx]
        d_w[d_idx] = d_w0[d_idx] + dt*d_aw[d_idx]


class HeatDiffusion(PressureGradApprox):
    def create_solver(self):
        integrator = RK2Integrator(dest=RK2StepperHD())
        return Solver(dim=self.dim,
                      pfreq=1000,
                      integrator=integrator,
                      tf=0.1,
                      dt=1e-6)

    def get_function(self, x, y):
        f = x * 0.0
        f[x<0.5] = 1.0
        f[x>=0.5] = -0.0
        # f = np.sin(2 * np.pi * x)
        return f

    def post_process(self, info):
        from pysph.solver.utils import load
        self.read_info(info)
        if len(self.output_files) == 0:
            return

        data = load(self.output_files[-1])
        dest = data['arrays']['dest']
        x = dest.x
        y = dest.y
        fc = None
        fc1 = None
        fe = None

        u = dest.u
        v = dest.v

        cond = (x > 0.5) & (x < 0.75) & (y > 0.5) & ( y < 0.75)
        plt.plot(u, label='comp')
        plt.legend()
        plt.show()
        filename = os.path.join(self.output_dir, 'results.npz')
        np.savez(filename, x=x, y=y, u=u)

    def pre_step(self, solver):
        from pysph.tools.interpolator import Interpolator
        fluid = self.particles[0]
        if solver.count < 1 and self.do_post==1:
            x = fluid.x
            cond = (x>0.25) & (x<0.75)
            x = x[cond]
            interp = Interpolator(self.particles, method='order1')
            
            interp.set_interpolation_points(x=x)
            u = interp.interpolate('u')
            fluid.u[cond] = u

    def post_step(self, solver):
        fluid = self.particles[0]
        if solver.count % 200 == 0:
            x = fluid.x
            u = fluid.u
            plt.clf()
            plt.plot(x, u)
            plt.pause(0.1)



if __name__ == "__main__":
    app = HeatDiffusion()
    app.run()
    app.post_process(app.info_filename)