from pysph.base.utils import get_particle_array
import numpy as np
from gresho_chan import GreshoChan

U = 1.0
L = 1.0
rho0 = 1.0
c0 = 10 * U
p0 = c0**2 * rho0


class GreshoChanWithBoundary(GreshoChan):
    def create_domain(self):
        return None

    def create_particles(self):
        # create the particles
        dx = self.dx
        nl = 5 * dx
        x, y, h = None, None, None

        _x = np.arange(-L/2+dx / 2, L/2, dx)
        x, y = np.meshgrid(_x, _x)
        _xs = np.arange(-L/2+dx / 2-nl, L/2+nl, dx)
        xs, ys = np.meshgrid(_xs, _xs)
        cond = ~((xs > -L/2) & (xs < L/2) & (ys > -L/2) & (ys < L/2))
        xs, ys = xs[cond], ys[cond]
        if self.options.init is not None:
            fname = self.options.init
            from pysph.solver.utils import load
            data = load(fname)
            _f = data['arrays']['fluid']
            x, y = _f.x.copy(), _f.y.copy()

        # Initialize
        m = self.volume * rho0
        V0 = self.volume
        if h is None:
            h = self.hdx * dx
        u0, v0, p0 = self.exact_solution(U, x, y)
        us0, vs0, ps0 = self.exact_solution(U, xs, ys)
        rhoc = None
        rhocs = None
        rho = rho0

        if self.options.eos == 'linear':
            print('linear')
            rho = (p0 / self.c0**2 + 1)
            rhoc = (p0 / self.c0**2 + 1)
            rhocs = (ps0 / self.c0**2 + 1)
        elif self.options.eos == 'tait':
            print('tait')
            rho = (p0 * 7.0 / self.c0**2 + 1)**(1./7.0)
            rhoc = (p0 * 7.0 / self.c0**2 + 1)**(1./7.0)
            rhocs = (ps0 * 7.0 / self.c0**2 + 1)**(1./7.0)

        # create the arrays
        print(m)
        fluid = get_particle_array(name='fluid', x=x, y=y, m=m, h=h, u=u0,
                                   v=v0, rho=rho, rhoc=rhoc, p=p0, V0=V0)
        print(len(xs), len(us0))
        channel = get_particle_array(
            name='channel', x=xs, y=ys, m=m, h=h,
            u=us0, v=vs0, rho=rho0, rhoc=rhocs, p=ps0, V0=V0
        )

        particles = [fluid, channel]
        self.scheme.setup_properties(particles)

        print("Taylor green vortex problem :: nfluid = %d, dt = %g" % (
            fluid.get_number_of_particles(), self.dt))

        from tg_config import configure_particles

        for pa in particles:
            configure_particles(self, pa)
            pa.add_constant('c0', self.c0)
            pa.add_property('rhoc')
            pa.add_property('normal', stride=3)
            pa.add_property('V')

        return particles

    def create_scheme(self):
        from tg_config import create_scheme
        return create_scheme(rho0, c0, p0, solids=['channel'])

    def create_equations(self):
        from tg_config import create_equation
        return create_equation(self, solids=['channel'])


if __name__ == '__main__':
    app = GreshoChanWithBoundary()
    app.run()
