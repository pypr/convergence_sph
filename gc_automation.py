
# autmation for gresho chan vortex problem
###
from automan.api import PySPHProblem
from automan.api import Simulation, filter_cases
import numpy as np
import matplotlib
matplotlib.use('pdf')

from automate import make_table
from tg_automation import get_label

BASE_HDX = 1.2
HDX = 1.2
RESOLUTIONS = [50, 100, 200, 250, 400, 500]
# RESOLUTIONS = [50, 100]
# RESOLUTIONS = [100, 200]
pfreq = 10000
CLEAN = True
NCORE, NTHREAD = 3, 10

def get_convergence_data(cases, nx='nx'):
    data = {}
    for case in cases:
        r = case.data['r']
        vmag = case.data['vmag']
        ve = case.data['ve1']
        cond = r < 0.5
        l1 = sum(abs(vmag[cond] - ve[cond]))/sum(ve[cond])
        data[case.params[nx]] = l1
    nx_arr = np.asarray(sorted(data.keys()), dtype=float)
    l1 = np.asarray([data[x] for x in nx_arr])
    return nx_arr, l1


class GCVConv(PySPHProblem):
    def __init__(self, sim, out, tf=2.0, perturb=0.2, common_cmd='', name='', cores=0, threads=0, res=RESOLUTIONS):
        self.tf = tf
        self.perturb = perturb
        self.cmd = common_cmd
        self.name = name + '_gc'
        self.cores = cores
        self.threads = threads
        self.res = res

        super(GCVConv, self).__init__(sim, out)

    def _make_cases(self, cmd, p_override=None, tf_override=None, hdx_override=0.0, res_override=None):
        get_path = self.input_path
        self.nx = self.res 
        self.kernel = 'QuinticSpline'
        self.hdx = HDX
        if hdx_override > 0.1:
            self.hdx = hdx_override

        tf = self.tf
        perturb=self.perturb
        if tf_override is not None:
            tf = tf_override
            perturb = p_override

        n_core = self.cores 
        n_thread = self.threads

        _cmd = 'python code/gresho_chan.py --openmp' + cmd + self.cmd
        cases = [
            Simulation(
                get_path('gc_nx_%d_%.1f' % (nx, perturb)), _cmd,
                job_info=dict(n_core=n_core, n_thread=n_thread), nx=nx,
                hdx=self.hdx, tf=tf
            )
            for nx in self.nx
        ]

        return cases

    def run(self):
        self.make_output_dir()
        self._plot_convergence_rate()
        self._plot_r_v()

    def _delete_folder(self):
        import shutil
        path = self.output_path()
        print("removing", path)
        shutil.rmtree(path, ignore_errors=True)

    def _plot_convergence_rate(self):
        import matplotlib.pyplot as plt
        style = ['g-o', 'b-o', 'r-o', 'k-0']
        plt.figure(1)
        plt.figure(2)
        cases = self.cases 
        nx, l1 = get_convergence_data(cases, nx='nx')
        plt.figure(1)
        plt.loglog(1.0/nx, l1, label='OC')
        hdx = self.hdx#get_new_hdx2(nx, BASE_HDX, 50)
        dx = 1.0/(nx) * hdx
        ldx = np.log(dx)
        ll1 = np.log(l1)
        slope = (ll1[1:] - ll1[:-1])/(ldx[1:] - ldx[:-1])
        plt.figure(2)
        plt.plot(dx[1:], slope, label='Order')

        plt.figure(1)
        # plt.loglog(1.0/nx, (5./nx)**2, 'k--', linewidth=2,
        #            label=r'Expected $O(h^2)$')
        # plt.loglog(1.0/nx, (5./nx), 'k:', linewidth=2,
        #            label=r'Expected $O(h)$')
        # plt.loglog(1.0/nx, np.power(5./nx, 0.5), 'k-.', linewidth=2,
        #            label=r'Expected $O(h^{0.5})$')
        plt.legend(loc='best')
        plt.xlabel(r'$h$')
        plt.ylabel(r'$L_1$ error')
        # plt.xlim(0.005, 0.1)
        plt.savefig(self.output_path('l1_conv.png'), dpi=300)
        plt.close(1)

        plt.figure(2)
        plt.legend(loc='best')
        plt.xlabel(r'$h$')
        plt.ylabel(r'Convergence order')
        # plt.xlim(0.005, 0.1)
        plt.savefig(self.output_path('l1_conv_order.png'), dpi=300)
        plt.close(2)

    def _plot_r_v(self):
        import matplotlib.pyplot as plt
        xe, ve = None, None
        for case in self.cases:
            nx = case.params['nx']
            data = case.data
            r = data['r']
            vmag = data['vmag']
            xe = data['xe']
            ve = data['ve']
            plt.plot(r, vmag, 'o', label="nx=%d"%nx, mfc='none')

        plt.plot(xe, ve, '--', label="exact")
        plt.grid()
        plt.xlabel('r')
        plt.ylabel(r'$|\mathbf{u}|$')
        plt.xlim([0, 0.5])
        plt.legend()
        plt.title('time = 3s')
        plt.savefig(self.output_path('r_vel.png'), dpi=300)
        plt.close()


class Comparison(PySPHProblem):
    def _make_case(self, klass, tf=2.0, perturb=0.0, common_cmd='', name='', threads=NTHREAD, cores=NCORE, res=RESOLUTIONS):
        self.tf = tf
        self.problems = {
            cls.__name__: cls(self.sim_dir, self.out_dir, tf=tf, perturb=perturb, common_cmd=common_cmd, name=name, cores=cores, threads=threads, res=res)
            for cls in klass
        }

    def _set_re(self, name):
        self.nx = self.problems[name].nx
        self.hdx = HDX

    def get_label(self, problem):
        return ''

    def get_requires(self):
        return list(self.problems.items())

    def run(self):
        self.make_output_dir()
        self._plot_convergence_rate()
        self._plot_r_v()
        self._plot_particles()
        self._plot_mom(name='mom')
        self._plot_mom(name='a_mom')

    def _plot_convergence_rate(self):
        import matplotlib.pyplot as plt
        available = (
            list('bgrcmyk') +
            ['darkred', 'chocolate', 'darkorange', 'lime', 'darkgreen',
             'fuchsia', 'navy', 'indigo']
        )
        colors = {
            scheme: available[i]
            for i, scheme in enumerate(sorted(self.problems.keys()))
        }

        max_nx = 0
        plt.figure(1)
        plt.figure(2)
        dxs = None
        for scheme, problem in self.problems.items():
            cases = problem.cases
            nx, l1 = get_convergence_data(cases, nx='nx')
            max_nx = max(nx.max(), max_nx)
            label = self.get_label(problem)
            hdx = self.hdx#get_new_hdx2(nx, BASE_HDX, 50)
            dx = 1.0/(nx) * hdx
            plt.figure(1)
            plt.loglog(dx, l1, color=colors[scheme],
                        linestyle='-', marker='o',
                        label=label)
            dxs = dx
            ldx = np.log(dx)
            ll1 = np.log(l1)
            slope = (ll1[1:] - ll1[:-1])/(ldx[1:] - ldx[:-1])
            plt.figure(2)
            plt.plot(dx[1:], slope, color=colors[scheme],
                        linestyle='-', marker='o',
                        label=label)

        # nx = np.linspace(10, max_nx, 5)
        plt.figure(1)
        print(dxs, l1)
        plt.loglog(dxs, l1[0]*(dxs/dxs[0])**2, 'k--', linewidth=2,
                    label=r'$O(h^2)$')
        plt.loglog(dxs, l1[0]*(dxs/dxs[0]), 'k:', linewidth=2,
                    label=r'$O(h)$')
        # plt.loglog(1.0/nx, l1[0]*np.power(1.0/nx*nx[0], 0.5), 'k-.', linewidth=2,
        #            label=r'Expected $O(h^{0.5})$')
        plt.legend(loc='best')
        plt.xlabel(r'$h$')
        plt.ylabel(r'$L_1$ error')
        plt.grid()
        plt.savefig(self.output_path(self.get_name() + '_conv.png'), bbox_inches='tight')
        plt.close(1)

        plt.figure(2)
        plt.legend(loc='best')
        plt.xlabel(r'$h$')
        plt.ylabel(r'Convergence order')
        plt.grid()
        plt.savefig(self.output_path(self.get_name() + '_l1_conv_order.png'), bbox_inches='tight')
        plt.close(2)

    def _plot_r_v(self):
        import matplotlib.pyplot as plt
        available = (
            list('bgrcmyk') +
            ['darkred', 'chocolate', 'darkorange', 'lime', 'darkgreen',
             'fuchsia', 'navy', 'indigo']
        )
        colors = {
            scheme: available[i]
            for i, scheme in enumerate(sorted(self.problems.keys()))
        }

        max_nx = 0
        plt.figure()
        xe, ve = None, None
        for scheme, problem in self.problems.items():
            cases = problem.cases
            case = filter_cases(cases, nx=100)
            data = case[0].data 
            label = self.get_label(problem)
            r = data['r']
            vmag = data['vmag']
            xe = data['xe']
            ve = data['ve']
            plt.plot(r, vmag, 'o', label=label, mfc='none')

        plt.plot(xe, ve, '--', label="exact")
        plt.grid()
        plt.xlabel('r')
        plt.ylabel(r'$|\mathbf{u}|$')
        plt.xlim([0, 0.5])
        plt.legend()
        plt.title('time = %.1fs'%self.tf)
        plt.savefig(self.output_path(self.get_name() + '_r_vel.png'), dpi=300)
        plt.close()


    def _plot_mom(self, name='mom'):
        import matplotlib.pyplot as plt
        available = (
            list('bgrcmyk') +
            ['darkred', 'chocolate', 'darkorange', 'lime', 'darkgreen',
             'fuchsia', 'navy', 'indigo']
        )
        colors = {
            scheme: available[i]
            for i, scheme in enumerate(sorted(self.problems.keys()))
        }

        max_nx = 0
        plt.figure()
        xe, ve = None, None
        for scheme, problem in self.problems.items():
            cases = problem.cases
            case = filter_cases(cases, nx=100)
            data = case[0].data 
            label = self.get_label(problem)
            t = data['time']
            mom = data[name]
            plt.semilogy(t, abs(mom), 'o-', label=label, mfc='none')

        plt.grid()
        plt.xlabel('time')
        if name == 'mom':
            plt.ylabel('Linear Momentum')
        else:
            plt.ylabel('Angular Momentum')
        plt.tight_layout()
        plt.legend()
        plt.savefig(self.output_path(self.get_name() + '_' + name + '.png'), dpi=300)
        plt.close()

    def _get_layout(self, n_case=3):
        import matplotlib.pyplot as plt
        from matplotlib import gridspec

        fig, axes = None, None
        if n_case == 3:
            max_nx = 0
            fig = plt.figure(figsize=(8, 8))
            grid = gridspec.GridSpec(nrows=4, ncols=4)
            axes = []
            axes.append(fig.add_subplot(grid[0:2, 0:2]))
            axes.append(fig.add_subplot(grid[2:4, 0:2]))
            axes[-1].yaxis.set_tick_params(labelbottom=False)
            axes.append(fig.add_subplot(grid[1:3, 2:4]))
        elif n_case == 4:
            max_nx = 0
            fig = plt.figure(figsize=(8, 8))
            grid = gridspec.GridSpec(nrows=4, ncols=4)
            axes = []
            axes.append(fig.add_subplot(grid[0:2, 0:2]))
            axes[-1].xaxis.set_tick_params(labelbottom=False)
            axes.append(fig.add_subplot(grid[2:4, 0:2]))
            axes.append(fig.add_subplot(grid[0:2, 2:4]))
            axes[-1].xaxis.set_tick_params(labelbottom=False)
            axes[-1].yaxis.set_tick_params(labelbottom=False)
            axes.append(fig.add_subplot(grid[2:4, 2:4]))
            axes[-1].yaxis.set_tick_params(labelbottom=False)
        return axes

    def _plot_particles(self):
        from pysph.solver.utils import get_files, load
        import matplotlib.pyplot as plt
        from matplotlib import gridspec

        axes = self._get_layout(len(self.problems.items()))
        index = 0
        xe, ve = None, None
        for scheme, problem in self.problems.items():
            cases = problem.cases
            case = filter_cases(cases, nx=100)
            files = get_files(case[0].input_path())
            print(files[-1])
            data = load(files[-1])['arrays']['fluid']
            x = data.x
            y = data.y
            p = data.p
            print(p, min(p), max(p))
            label = self.get_label(problem)
            axes[index].scatter(x, y, c=p/max(p), s=5.0) 
            axes[index].text(0.1, 0.1, label, fontsize=20, transform=axes[index].transAxes)
            index += 1

        plt.tight_layout(pad=1.0)
        plt.savefig(self.output_path(self.get_name() + '_p.png'), dpi=300)
        plt.close()

class TSPH(GCVConv):
    def get_name(self):
        return 'tsph' + self.name

    def setup(self):
        cmd = (
            ' --scheme tsph --method sd --scm wcsph --pst-freq 10 --damp-pre '
        )
        self.cases = self._make_cases(cmd)


class TSPHFatehi(GCVConv):
    def get_name(self):
        return 'tsph_fatehi' + self.name

    def setup(self):
        cmd = (
            ' --scheme tsph --method sd --scm fatehi --pst-freq 10 --damp-pre'
        )
        self.cases = self._make_cases(cmd)


class RemeshTSPH(GCVConv):
    def get_name(self):
        return 'remesh_tsph' + self.name

    def setup(self):
        cmd = (
            ' --scheme rsph --remesh 1 --remesh-eq m4 --damp-pre '
        )
        self.cases = self._make_cases(cmd, hdx_override=1.0)


class TEDAC(GCVConv):
    def get_name(self):
        return 'tedac' + self.name

    def setup(self):
        cmd = (
            ' --scheme tsph --method no_sd --scm edac --pst-freq 10 --damp-pre '
        )
        self.cases = self._make_cases(cmd)


class EWCSPH(GCVConv):
    def get_name(self):
        return 'ewcsph' + self.name

    def setup(self):
        cmd = (
            ' --scheme ewcsph --method org --eos linear '
        )
        self.cases = self._make_cases(cmd)


class EWCSPHSOC(GCVConv):
    def get_name(self):
        return 'ewcsph_soc' + self.name

    def setup(self):
        cmd = (
            ' --scheme ewcsph --method soc --eos linear '
        )
        self.cases = self._make_cases(cmd)

class TDSPH(GCVConv):
    def get_name(self):
        return 'tdsph' + self.name

    def setup(self):
        cmd = (
            ' --scheme tdsph --split-cal --eos linear '
        )
        self.cases = self._make_cases(cmd)


class TDSPH2(GCVConv):
    def get_name(self):
        return 'tdsph2' + self.name

    def setup(self):
        cmd = (
            ' --scheme tdsph --eos linear '
        )
        self.cases = self._make_cases(cmd)


class TSPHDPPST(GCVConv):
    def get_name(self):
        return 'tsph_dppst' + self.name

    def setup(self):
        cmd = (
            ' --scheme tsph --scm wcsph --pst-freq 10 --pst dppst --damp-pre '
        )
        self.cases = self._make_cases(cmd)


class DPSPH(GCVConv):
    def get_name(self):
        return 'dpsph' + self.name

    def setup(self):
        cmd = (
            ' --scheme delta_plus --velocity-correction '
            ' --variable-umax '
        )
        self.cases = self._make_cases(cmd)


class TVF(GCVConv):
    def get_name(self):
        return 'tvf' + self.name

    def setup(self):
        cmd = (
            ' --scheme tvf '
        )
        self.cases = self._make_cases(cmd)


class GTVF(GCVConv):
    def get_name(self):
        return 'gtvf' + self.name

    def setup(self):
        cmd = (
            ' --scheme gtvf '
        )
        self.cases = self._make_cases(cmd)


class EDAC(GCVConv):
    def get_name(self):
        return 'edac' + self.name

    def setup(self):
        cmd = (
            ' --scheme edac '
        )
        self.cases = self._make_cases(cmd)


comp1 = [TVF, DPSPH, EDAC, TSPH]
comp_var = [TSPH, TEDAC, TSPHDPPST]
not_running = [RemeshTSPH, EWCSPHSOC, TDSPH, EWCSPH]

# -------------------------------------------------------------------------
# Comparison of the different schemes.

class ComparisonSchemesGC(Comparison):
    def get_name(self):
        return 'scheme_compare_gc'

    def get_label(self, problem):
        return get_label(problem)

    def setup(self):
        cmd = ' --timestep 0.0000654 '
        # cmd = ' '
        self._make_case(comp1, tf=3.0, common_cmd=cmd)
        self._set_re('TSPH')


class ComparisonVariationGC(Comparison):
    def get_name(self):
        return 'compare_variation_gc'

    def get_label(self, problem):
        return get_label(problem)

    def setup(self):
        cmd = ' --timestep 0.0000654  '
        # cmd = ' '
        self._make_case(comp_var, tf=3.0, common_cmd=cmd)
        self._set_re('TEDAC')


class ComparisonVariationGCNW(Comparison):
    def get_name(self):
        return 'compare_variation_gc_nw'

    def get_label(self, problem):
        return get_label(problem)

    def setup(self):
        cmd = ' --timestep 0.0000654  '
        # cmd = ' '
        self._make_case(not_running, tf=1.5, common_cmd=cmd, res=[100])
        self._set_re('TDSPH')