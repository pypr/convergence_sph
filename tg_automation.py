# autmation for taylor green problem
###
from automan.api import PySPHProblem
from automan.api import Simulation, filter_cases
import numpy as np
import matplotlib
matplotlib.use('pdf')

from automate import make_table

BASE_HDX = 1.2
HDX = 1.2
RESOLUTIONS = [50, 100, 200, 250, 400, 500]
# RESOLUTIONS = [50, 100]
# RESOLUTIONS = [100, 200]
pfreq = 1000
CLEAN = True
NCORE, NTHREAD = 3, 10

def get_convergence_data(cases, nx='nx', error='l1'):
    data = {}
    for case in cases:
        l1 = case.data[error]
        l1 = sum(l1)/len(l1)
        data[case.params[nx]] = l1
    nx_arr = np.asarray(sorted(data.keys()), dtype=float)
    l1 = np.asarray([data[x] for x in nx_arr])
    return nx_arr, l1


def get_time_taken(cases, nx='nx'):
    from automate import get_cpu_time
    data = {}
    for case in cases:
        time = get_cpu_time(case)
        cmd = case.base_command.split()
        if not 'code/taylor_green_c0_80.py' in cmd:
            time /= 1.5
        if 'delta_plus' in cmd:
            time /= 1.5
        data[case.params[nx]] = time
    nx_arr = np.asarray(sorted(data.keys()), dtype=float)
    times = np.asarray([data[x] for x in nx_arr])
    return nx_arr, times

def get_label(problem):
    params = problem.cases[0].base_command.split()

    if "tvf" in params:
        return 'TVF'
    elif 'ewcsph' in params:
        if 'soc' in params:
            return 'E-C'
        else:
            return 'EWCSPH'
    elif 'delta_plus' in params:
        return r'$\delta^{+}SPH$'
    elif 'tsph' in params:
        if 'fatehi' in params:
            return 'L-IPST-F'
        elif 'dppst' in params:
            return 'L-PST-C'
        elif 'edac' in params:
            return 'PE-IPST-C'
        else:
            return 'L-IPST-C'
    elif 'tdsph' in params:
        if '--split-cal' in params:
            return 'TV-C'
        else:
            return 'TV-C2'
    elif 'rsph' in params:
        return 'L-RR-C'
    elif 'edac' in params:
        return 'EDAC'

class TGVConv(PySPHProblem):
    def __init__(self, sim, out, tf=2.0, perturb=0.2, common_cmd='', name='', cores=0, threads=0, res=RESOLUTIONS):
        self.tf = tf
        self.perturb = perturb
        self.cmd = common_cmd
        self.name = name
        self.cores = cores
        self.threads = threads
        self.res = res

        super(TGVConv, self).__init__(sim, out)

    def _get_files(self):
        return ' code/taylor_green.py ' 

    def _make_cases(self, cmd, p_override=None, tf_override=None, hdx_override=0.0):
        get_path = self.input_path
        self.nx = self.res 
        self.re = [100]#, 1000] # , 10000]
        self.kernel = 'QuinticSpline'
        self.hdx = HDX
        if hdx_override > 0.1:
            self.hdx = hdx_override

        tf = self.tf
        perturb=self.perturb
        if tf_override is not None:
            tf = tf_override
            perturb = p_override

        n_core = self.cores 
        n_thread = self.threads

        _cmd = 'python' +  self._get_files() + '--openmp' + cmd + self.cmd
        cases = [
            Simulation(
                get_path('re_%s_nx_%d_%.1f' % (re, nx, perturb)), _cmd,
                job_info=dict(n_core=n_core, n_thread=n_thread), nx=nx, re=re, tf=tf,
                perturb=perturb, kernel=self.kernel,
                hdx=self.hdx #get_new_hdx2(nx, BASE_HDX, 50, 450, 0.3)
            )
            for re in self.re for nx in self.nx
        ]

        return cases

    def run(self):
        self.make_output_dir()
        self._plot_convergence_rate()
        self._plot_errors_with_time(error='l1')
        self._plot_errors_with_time(error='p_l1')

    def _delete_folder(self):
        import shutil
        path = self.output_path()
        print("removing", path)
        shutil.rmtree(path, ignore_errors=True)

    def _plot_errors_with_time(self, error='l1'):
        import matplotlib.pyplot as plt
        style = ['g-o', 'b-o', 'r-o', 'k-o', 'c-o']
        for re in self.re:
            cases = filter_cases(self.cases, re=re)
            plt.figure()
            for case, ls in zip(cases, style):
                data = case.data
                nx = case.params['nx']
                l1 = data[error]
                t = data['t']
                plt.semilogy(t[1:], l1[1:], ls, label='nx=%d'%nx)
            plt.grid()
            plt.xlabel('t')
            plt.ylabel(r'$L_1$ error')
            plt.legend()
            plt.savefig(self.output_path('%d_%s.pdf'%(re, error)))
            plt.close()

    def _plot_convergence_rate(self):
        import matplotlib.pyplot as plt
        style = ['g-o', 'b-o', 'r-o', 'k-0']
        plt.figure(1)
        plt.figure(2)
        for re, ls in zip(self.re, style):
            cases = filter_cases(self.cases, re=re)
            nx, l1 = get_convergence_data(cases, nx='nx', error='l1')
            plt.figure(1)
            plt.loglog(1.0/nx, l1, ls, label='Re=%s' % re)
            hdx = self.hdx#get_new_hdx2(nx, BASE_HDX, 50)
            dx = 1.0/(nx)*hdx
            ldx = np.log(dx)
            ll1 = np.log(l1)
            slope = (ll1[1:] - ll1[:-1])/(ldx[1:] - ldx[:-1])
            plt.figure(2)
            plt.plot(dx[1:], slope, ls, label='Re=%s' % re)

        plt.figure(1)
        # plt.loglog(1.0/nx, (5./nx)**2, 'k--', linewidth=2,
        #            label=r'Expected $O(h^2)$')
        # plt.loglog(1.0/nx, (5./nx), 'k:', linewidth=2,
        #            label=r'Expected $O(h)$')
        # plt.loglog(1.0/nx, np.power(5./nx, 0.5), 'k-.', linewidth=2,
        #            label=r'Expected $O(h^{0.5})$')
        plt.legend(loc='best')
        plt.xlabel(r'$h$')
        plt.ylabel(r'$L_1$ error')
        # plt.xlim(0.005, 0.1)
        plt.savefig(self.output_path('l1_conv.pdf'))
        plt.close(1)

        plt.figure(2)
        plt.legend(loc='best')
        plt.xlabel(r'$h$')
        plt.ylabel(r'Convergence order')
        # plt.xlim(0.005, 0.1)
        plt.savefig(self.output_path('l1_conv_order.pdf'))
        plt.close(2)


class Comparison(PySPHProblem):
    def _make_case(self, klass, tf=2.0, perturb=0.0, common_cmd='', name='', threads=NTHREAD, cores=NCORE, res=RESOLUTIONS):
        self.problems = {
            cls.__name__: cls(self.sim_dir, self.out_dir, tf=tf, perturb=perturb, common_cmd=common_cmd, name=name, cores=cores, threads=threads, res=res)
            for cls in klass
        }

    def _set_re(self, name):
        self.re = self.problems[name].re
        self.nx = self.problems[name].nx
        self.hdx = HDX

    def get_label(self, problem):
        return ''

    def get_requires(self):
        return list(self.problems.items())

    def run(self):
        self.make_output_dir()
        self._plot_convergence_rate()
        # self._plot_convergence_rate(error='p_l1')
        self.save_table(self.re[0], 'l1')
        self.save_table(self.re[0], 'p_l1')
        self.save_table_time_lm(self.re[0])
        # self.remove_data()

    def remove_data(self):
        if CLEAN:
            import glob
            import os
            for problem in self.problems.values():
                cases = problem.cases
                for case in cases:
                    dirname = case.input_path()
                    path = os.path.join(dirname , 'taylor_green*.hdf5')
                    names0 = glob.glob(path)
                    path = os.path.join(dirname , 'taylor_green*.npz')
                    names1 = glob.glob(path)
                    names = names0 + names1
                    for name in names:
                        print("removing", name)
                        os.remove(name)

    def _plot_convergence_rate(self):
        import matplotlib.pyplot as plt
        available = (
            list('bgrcmyk') +
            ['darkred', 'chocolate', 'darkorange', 'lime', 'darkgreen',
             'fuchsia', 'navy', 'indigo']
        )
        colors = {
            scheme: available[i]
            for i, scheme in enumerate(sorted(self.problems.keys()))
        }

        # If you want to do this manually
        # colors = {
        #     'WCSPH': 'k', 'EDAC': 'r', 'TVF': 'b'
        # }

        max_nx = 0
        fig, axes = plt.subplots(1, 2, sharey=True, figsize=(10, 5))
        print(axes)
        errors = ['p_l1', 'l1']
        for i, error in enumerate(errors):
            for re in self.re:
                dx = None
                for scheme, problem in self.problems.items():
                    cases = filter_cases(problem.cases, re=re)
                    nx, l1 = get_convergence_data(cases, nx='nx', error=error)
                    max_nx = max(nx.max(), max_nx)
                    label = self.get_label(problem)
                    hdx = self.hdx#get_new_hdx2(nx, BASE_HDX, 50)
                    dx = 1.0/(nx)*hdx
                    axes[i].loglog(dx, l1, color=colors[scheme],
                            linestyle='-', marker='o',
                            label=label)

                # nx = np.linspace(10, max_nx, 5)
                print(dx)
                axes[i].loglog(dx, l1[0]*(dx/dx[0])**2, 'k--', linewidth=2,
                        label=r'$O(h^2)$')
                axes[i].loglog(dx, l1[0]*(dx/dx[0]), 'k:', linewidth=2,
                        label=r'$O(h)$')
                axes[i].set_xlabel(r'$h$')
                if error == 'p_l1':
                    axes[i].text(0.85, 0.05, 'Pressure', transform=axes[i].transAxes)
                elif error == 'l1':
                    axes[i].text(0.85, 0.05, 'Velocity', transform=axes[i].transAxes)
                if i==0: 
                    axes[i].set_ylabel(r'$L_1$ error')
                axes[i].grid()

        handles, labels = axes[0].get_legend_handles_labels()
        fig.legend(handles, labels, bbox_to_anchor=[0.5, 0.92], ncol=5, loc='center')
        plt.tight_layout()
        plt.subplots_adjust(top=0.83, hspace=0.0, wspace=0.0)
        plt.savefig(self.output_path(self.get_name() + '_conv_re_%s.png' %(re)))
        plt.close()

    def save_table(self, re, error):
        column_names = ['Resolution']
        colformat = 'c'

        rows = []
        for n in self.nx:
            hdx = self.hdx#get_new_hdx2(n, BASE_HDX, 50)
            row = [int(n)]
            rows.append(row)

        for scheme, problem in self.problems.items():
            colformat += 'l'
            column_names.append(self.get_label(problem))
            cases = filter_cases(problem.cases, re=re)
            nx, l1 = get_convergence_data(cases, nx='nx', error=error)
            for i, row in enumerate(rows):
                if i == 0:
                    row.append('%.2e'%l1[i])
                else:
                    l0 = l1[i-1]
                    l = l1[i]
                    N0 = nx[i - 1]
                    hdx = self.hdx#get_new_hdx2(N0, BASE_HDX, 50)
                    # N0 /= hdx
                    N = nx[i]
                    hdx = self.hdx#get_new_hdx2(N, BASE_HDX, 50)
                    # N /= hdx
                    order = -(np.log(l / l0)) / (np.log(N / N0))
                    row.append('%.2e(%.2f)'%(l1[i], order))

        print(column_names)
        print(rows)
        make_table(column_names,
                   rows,
                   self.output_path(self.get_name() + '_%.1f_%s_err.tex' %
                                    (re, error)),
                   sort_cols=[0],
                   column_format=colformat)

    def _get_order(self, nx, l1):
        # obtaining linear regression coefficient
        # https://www.geeksforgeeks.org/linear-regression-python-implementation/
        nx = np.log(nx)
        l1 = np.log(l1)
        n = np.size(nx)
  
        # mean of x and y vector
        m_x = np.mean(nx)
        m_y = np.mean(l1)
    
        # calculating cross-deviation and deviation about x
        SS_xy = np.sum(l1*nx) - n*m_y*m_x
        SS_xx = np.sum(nx*nx) - n*m_x*m_x
    
        # calculating regression coefficients
        order = -SS_xy / SS_xx

        return order

    def save_table_time_lm(self, re):
        column_names = [
            'Name', r'$\frac{F_T}{F_{max}}$', r'$T_r$', r'$L_1(|\ten{u}|)(O)$',
            r'$L_1(p)(O)$'
        ]
        colformat = 'cllll'

        rows = []
        min_time = 1000000.
        for scheme, problem in self.problems.items():
            row = []
            row.append(self.get_label(problem))
            cases = filter_cases(problem.cases, re=re)
            nx, l1 = get_convergence_data(cases, nx='nx', error='l1')
            nx, p_l1 = get_convergence_data(cases, nx='nx', error='p_l1')
            nx, lm = get_convergence_data(cases, nx='nx', error='lm')
            nx, time = get_time_taken(cases, nx='nx')
            order_l1 = self._get_order(nx, l1)
            order_p_l1 = self._get_order(nx, p_l1)
            for i, _nx in enumerate(nx):
                if abs(nx[i] - 500) < 0.01:
                    if time[i] < min_time:
                        min_time = time[i]
                    row.append('%.2e'%lm[i])
                    row.append(time[i])
                    # order = -(np.log(l1[-1] / l1[-2])) / (np.log(nx[-1] / nx[-2]))
                    row.append('%.2e(%.2f)'%(l1[-1], order_l1))
                    # order = -(np.log(p_l1[-1] / p_l1[-2])) / (np.log(nx[-1] / nx[-2]))
                    row.append('%.2e(%.2f)'%(p_l1[-1], order_p_l1))
            rows.append(row)

        print(column_names)
        print(rows)
        for row in rows:
            row[-3] /= min_time
            row[-3] = '%.2f'%row[-3]
        make_table(column_names,
                   rows,
                   self.output_path(self.get_name() + '_%.1f_time.tex' %
                                    (re)),
                   sort_cols=[0],
                   column_format=colformat)


class CompareCost(Comparison):
    def run(self):
        self.make_output_dir()
        self._plot_cost()
        # self.remove_data()

    def _plot_cost(self):
        import matplotlib.pyplot as plt
        available = (
            list('bgrcmyk') +
            ['darkred', 'chocolate', 'darkorange', 'lime', 'darkgreen',
             'fuchsia', 'navy', 'indigo']
        )
        colors = {
            scheme: available[i]
            for i, scheme in enumerate(sorted(self.problems.keys()))
        }

        # If you want to do this manually
        # colors = {
        #     'WCSPH': 'k', 'EDAC': 'r', 'TVF': 'b'
        # }

        max_nx = 0
        fig, axes = plt.subplots(1, 1, sharey=True, figsize=(10, 7))
        print(axes)
        errors = ['l1']
        for i, error in enumerate(errors):
            for re in self.re:
                dx = None
                for scheme, problem in self.problems.items():
                    cases = filter_cases(problem.cases, re=re)
                    nx, l1 = get_convergence_data(cases, nx='nx', error=error)
                    nx, time = get_time_taken(cases, nx='nx')
                    label = self.get_label(problem)
                    axes.loglog(time, l1, color=colors[scheme],
                            linestyle='-', marker='o',
                            label=label)

                # nx = np.linspace(10, max_nx, 5)
                axes.set_xlabel(r'$time$')
                axes.set_ylabel(r'$L_1$ error')
                axes.grid()

        handles, labels = axes.get_legend_handles_labels()
        fig.legend(handles, labels, bbox_to_anchor=[0.5, 0.9], ncol=5, loc='center')
        plt.tight_layout()
        plt.subplots_adjust(top=0.8, hspace=0.0, wspace=0.0)
        plt.savefig(self.output_path(self.get_name() + '.png'))
        plt.close()


class TSPH(TGVConv):
    def get_name(self):
        return 'tsph' + self.name

    def setup(self):
        cmd = (
            ' --scheme tsph --method sd --scm wcsph --pst-freq 10'
        )
        self.cases = self._make_cases(cmd)


class TSPHFatehi(TGVConv):
    def get_name(self):
        return 'tsph_fatehi' + self.name

    def setup(self):
        cmd = (
            ' --scheme tsph --method sd --scm fatehi --pst-freq 10'
        )
        self.cases = self._make_cases(cmd)


class TSPH2(TGVConv):
    def get_name(self):
        return 'tsph2' + self.name

    def setup(self):
        cmd = (
            ' --scheme tsph --method sd --scm wcsph --pst-freq 10 --c0-fac 20 --timestep 0.0000342 '
        )
        self.cases = self._make_cases(cmd)


class TSPH4(TGVConv):
    def get_name(self):
        return 'tsph4' + self.name

    def setup(self):
        cmd = (
            ' --scheme tsph --method sd --scm wcsph --pst-freq 10 --c0-fac 40 --timestep 0.0000175 '
        )
        self.cases = self._make_cases(cmd)


class TSPH8(TGVConv):
    def get_name(self):
        return 'tsph8' + self.name

    def setup(self):
        cmd = (
            ' --scheme tsph --method sd --scm wcsph --pst-freq 10 --c0-fac 80 --timestep 0.0000088 '
        )
        self.cases = self._make_cases(cmd)


class TSPHFatehi2(TGVConv):
    def get_name(self):
        return 'tsph_fatehi2' + self.name

    def setup(self):
        cmd = (
            ' --scheme tsph --method sd --scm fatehi --pst-freq 10 --c0-fac 20 --timestep 0.0000342 '
        )
        self.cases = self._make_cases(cmd)


class TSPHFatehi4(TGVConv):
    def get_name(self):
        return 'tsph_fatehi4' + self.name

    def setup(self):
        cmd = (
            ' --scheme tsph --method sd --scm fatehi --pst-freq 10 --c0-fac 40 --timestep 0.0000175 '
        )
        self.cases = self._make_cases(cmd)


class TSPHFatehi8(TGVConv):
    def get_name(self):
        return 'tsph_fatehi8' + self.name

    def setup(self):
        cmd = (
            ' --scheme tsph --method sd --scm fatehi --pst-freq 10 --c0-fac 80 --timestep 0.0000088 '
        )
        self.cases = self._make_cases(cmd)


class TSPHLinear(TGVConv):
    def get_name(self):
        return 'tsph_linear' + self.name

    def setup(self):
        cmd = (
            ' --scheme tsph --method sd --scm wcsph --pst-freq 10 --c0-fac 80 --eos linear'
        )
        self.cases = self._make_cases(cmd)


class RemeshTSPH(TGVConv):
    def get_name(self):
        return 'remesh_tsph' + self.name

    def setup(self):
        cmd = (
            ' --scheme rsph --remesh 1 --remesh-eq m4 --eos linear '
        )
        self.cases = self._make_cases(cmd, hdx_override=1.0)


class RemeshTSPH8(TGVConv):
    def get_name(self):
        return 'remesh_tsph8' + self.name

    def setup(self):
        cmd = (
            ' --scheme rsph --remesh 1 --remesh-eq m4 --c0-fac 80 --eos linear '
        )
        self.cases = self._make_cases(cmd, hdx_override=1.0)

class TEDAC(TGVConv):
    def get_name(self):
        return 'tedac' + self.name

    def setup(self):
        cmd = (
            ' --scheme tsph --method no_sd --scm edac --pst-freq 10 --c0-fac 80 '
        )
        self.cases = self._make_cases(cmd)


class EWCSPH(TGVConv):
    def get_name(self):
        return 'ewcsph' + self.name

    def setup(self):
        cmd = (
            ' --scheme ewcsph --method org --eos linear '
        )
        self.cases = self._make_cases(cmd)


class EWCSPH8(TGVConv):
    def get_name(self):
        return 'ewcsph_80' + self.name

    def setup(self):
        cmd = (
            ' --scheme ewcsph --method org --c0-fac 80 --eos tait '
        )
        self.cases = self._make_cases(cmd)


class EWCSPH4(TGVConv):
    def get_name(self):
        return 'ewcsph_40' + self.name

    def setup(self):
        cmd = (
            ' --scheme ewcsph --method org --c0-fac 40 --eos linear '
        )
        self.cases = self._make_cases(cmd)


class EWCSPH2(TGVConv):
    def get_name(self):
        return 'ewcsph_20' + self.name

    def setup(self):
        cmd = (
            ' --scheme ewcsph --method org --c0-fac 20 --eos linear '
        )
        self.cases = self._make_cases(cmd)


class EWCSPHSOC(TGVConv):
    def get_name(self):
        return 'ewcsph_soc' + self.name

    def setup(self):
        cmd = (
            ' --scheme ewcsph --method soc --eos linear '
        )
        self.cases = self._make_cases(cmd)


class EWCSPHSOC8(TGVConv):
    def get_name(self):
        return 'ewcsph_soc80' + self.name

    def setup(self):
        cmd = (
            ' --scheme ewcsph --method soc --c0-fac 80 --eos linear '
        )
        self.cases = self._make_cases(cmd)


class EWCSPHSOC4(TGVConv):
    def get_name(self):
        return 'ewcsph_soc40' + self.name

    def setup(self):
        cmd = (
            ' --scheme ewcsph --method soc --c0-fac 40 --eos linear '
        )
        self.cases = self._make_cases(cmd)


class EWCSPHSOC2(TGVConv):
    def get_name(self):
        return 'ewcsph_soc20' + self.name

    def setup(self):
        cmd = (
            ' --scheme ewcsph --method soc --c0-fac 20 --eos linear '
        )
        self.cases = self._make_cases(cmd)


class TDSPH(TGVConv):
    def get_name(self):
        return 'tdsph' + self.name

    def setup(self):
        cmd = (
            ' --scheme tdsph --split-cal --c0-fac 80 --eos linear '
        )
        self.cases = self._make_cases(cmd)


class TDSPH2(TGVConv):
    def get_name(self):
        return 'tdsph2' + self.name

    def setup(self):
        cmd = (
            ' --scheme tdsph --c0-fac 80 --eos linear '
        )
        self.cases = self._make_cases(cmd)


class TSPHDPPST(TGVConv):
    def get_name(self):
        return 'tsph_dppst' + self.name

    def setup(self):
        cmd = (
            ' --scheme tsph --method sd --scm wcsph --pst-freq 10 --c0-fac 80 --pst dppst '
        )
        self.cases = self._make_cases(cmd)


class DPSPH(TGVConv):
    def get_name(self):
        return 'dpsph' + self.name

    def setup(self):
        cmd = (
            ' --scheme delta_plus --velocity-correction '
            ' --variable-umax '
        )
        self.cases = self._make_cases(cmd)


class TVF(TGVConv):
    def get_name(self):
        return 'tvf' + self.name

    def setup(self):
        cmd = (
            ' --scheme tvf '
        )
        self.cases = self._make_cases(cmd)


class EDAC(TGVConv):
    def get_name(self):
        return 'edac' + self.name

    def setup(self):
        cmd = (
            ' --scheme edac '
        )
        self.cases = self._make_cases(cmd)


class TVF80(TGVConv):
    def get_name(self):
        return 'tvf_80' + self.name

    def setup(self):
        cmd = (
            ' --scheme tvf --c0-fac 80 '
        )
        self.cases = self._make_cases(cmd)

    def _get_files(self):
        return ' code/taylor_green_c0_80.py '

class EDAC80(TGVConv):
    def get_name(self):
        return 'edac_80' + self.name

    def setup(self):
        cmd = (
            ' --scheme edac --c0-fac 80 '
        )
        self.cases = self._make_cases(cmd)

    def _get_files(self):
        return ' code/taylor_green_c0_80.py '

class DPSPH80(TGVConv):
    def get_name(self):
        return 'dpsph_80' + self.name

    def setup(self):
        cmd = (
            ' --scheme delta_plus --velocity-correction '
            ' --variable-umax --c0-fac 80 '
        )
        self.cases = self._make_cases(cmd)

    def _get_files(self):
        return ' code/taylor_green_c0_80.py '


tsph = [TSPH2, TSPH4, TSPH8, TSPHFatehi2, TSPHFatehi4, TSPHFatehi8]
ewcsph = [EWCSPH8, EWCSPH4, EWCSPH2, EWCSPHSOC8, EWCSPHSOC4, EWCSPHSOC2]
comp1 = [TVF, DPSPH, EDAC, TSPH, EWCSPH]
compeos = [TSPH8, TSPHLinear]
comp_var = [TSPH8, TEDAC, TDSPH, TSPHDPPST, EWCSPHSOC8, RemeshTSPH8, EWCSPH8]
comp_acc_vs_time = [TVF80, DPSPH80, EDAC80, EWCSPH8, TSPH8, TEDAC, TDSPH, TSPHDPPST, EWCSPHSOC8, RemeshTSPH8, TSPHFatehi8]

# -------------------------------------------------------------------------
# Comparison of the different schemes.
class ComparisonSchemes(Comparison):
    def get_name(self):
        return 'scheme_compare'

    def get_label(self, problem):
        return get_label(problem)

    def setup(self):
        cmd = ' --max-step 1 --timestep 0.0000654 '
        self._make_case(comp1, tf=0.5, common_cmd=cmd, cores=2, threads=4)
        self._set_re('TSPH')


class ComparisonC0(Comparison):
    def get_name(self):
        return 'compare_c0'

    def get_label(self, problem):
        params = problem.cases[0].base_command.split()

        print(params)
        if 'wcsph' in params:
            if params[-5] == '20':
                return r'L-IPST-C $c_o=20$'
            elif params[-5] == '40':
                return r'L-IPST-C $c_o=40$'
            elif params[-5] == '80':
                return r'L-IPST-C $c_o=80$'
        elif 'fatehi' in params:
            if params[-5] == '20':
                return r'L-IPST-F $c_o=20$'
            elif params[-5] == '40':
                return r'L-IPST-F $c_o=40$'
            elif params[-5] == '80':
                return r'L-IPST-F $c_o=80$'

    def setup(self):
        cmd = ' --pfreq 1000 '
        self._make_case(tsph, tf=0.5, common_cmd=cmd)
        self._set_re('TSPH2')


class ComparisonCostAll(CompareCost):
    def get_name(self):
        return 'compare_cost'

    def get_label(self, problem):
        return get_label(problem)

    def setup(self):
        cmd = ' --max-step 5000 --pfreq 200 --timestep 0.0000225'
        self._make_case(comp_acc_vs_time, tf=0.05, common_cmd=cmd, name='_cost', res=[50, 100, 200], threads=1, cores=1)
        self._set_re('TSPH8')

class ComparisonVariation(Comparison):
    def get_name(self):
        return 'compare_variation'

    def get_label(self, problem):
        return get_label(problem)

    def setup(self):
        cmd = ' --timestep 0.0000088 --pfreq 1000 '
        self._make_case(comp_var, tf=0.5, common_cmd=cmd)
        self._set_re('TEDAC')
# -------------------------------------------------------------------------
# Comparison of the different schemes.